#
# Required
#
variable "vpod_name" {
  type = string
}

variable "network_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "csv" {
  type = string
}

#
# Optional
#

variable "dns_base" {
  type    = string
  default = "openstack.example.net"
}
