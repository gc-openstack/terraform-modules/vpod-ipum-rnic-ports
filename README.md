# vpod-ipum-rnic-ports
Terraform Module: vpod-ipum-rnic-ports
======================================

This module works in a similar fashion to [ipum-pod64-ports](https://gitlab.com/gc-openstack/terraform-modules/ipum-pod64-ports), but instead creates the RNIC interfaces on a per vpod level, rather than at a global level.

The module uses a provided CSV file (rnic_podXX.csv) that include the interface name, mac address and IP of the interface.   

Eg:
    hostname,mac,ip
    lr8_ipum1mx,0c:42:a1:78:8b:15,10.5.24.1
    lr8_ipum2mx,04:3f:72:eb:45:25,10.5.24.2

This will create Neutron ports for the IPUM RDMA NICs.   This will generate DHCP configuration that will then be consumed by the IPUMs (although the port will show as 'detached')


Variables
=========
Required
--------
* vpod_name - the name of the vpod
* network_id - the openstack network the ports should be created on
* subnet_id - the subnet that the RNIC ports should be attached to
* csv - the path to the CSV file describing the RNIC ports

Optional
--------
* dns_base - the DNS domain used for these ports (default: openstack.example.net)

Other Inputs
============
none

Usage
=====
The module is used per vPOD

e.g.

    module "ipum8a" {
    source     = "git::ssh://git@gitlab.com/gc-openstack/terraform-modules/vpod-ipum-rnic-ports.git"
    vpod_name  = var.vpod_name
    csv        = var.ipum_csv
    network_id = module.networks.rnic_network_id
    subnet_id  = module.networks.rnic_subnet_id
    }

where the tfvars are:

    vpod_name         = "vpod7"
    ctrl_vlan         = 1234
    rnic_vlan         = 2345
    storage_vlan      = 3456
    cidr_hint         = 100
    ipum_csv          = "rnic_rack8.csv"
    vpod_floatingip   = "1.2.3.4"
    poplaraz          = "rack8-AZ"
    assigned_ipums    = ["rack8-ipum1", "rack8-ipum2", "rack8-ipum3", "rack8-ipum4", "rack8-ipum5", "rack8-ipum6", "rack8-ipum7", "rack8-ipum8", "rack8-ipum9", "rack8-ipum10", "rack8-ipum11", "rack8-ipum12", "rack8-ipum13", "rack8-ipum14", "rack8-ipum15", "rack8-ipum16" ]
    flavor            = "r6525.full" # flavor used for Poplar hosts only
    image             = "ubuntu-18.04-mlnx"

Development and Feedback
========================
The development of this tooling is ongoing, and any feedback is appreciated.  If you find any errors or bugs, please create an issue on the appropriate repository, or contact your Graphcore representative.