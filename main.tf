terraform {
  required_version = ">= 1.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.35"
    }
    local = {
      version = ">= 1.4"
    }
  }
}

locals {
  rnic = csvdecode(file(var.csv))
}

resource "openstack_networking_port_v2" "rnic_ports" {
  for_each = { for mapping in local.rnic :
  mapping.hostname => mapping if mapping.mac != "" }

  name        = replace(each.value.hostname, "_", "-")
  dns_name    = replace(each.value.hostname, "_", "-")
  network_id  = var.network_id
  mac_address = each.value.mac
  fixed_ip {
    subnet_id  = var.subnet_id
    ip_address = each.value.ip
  }
  admin_state_up = "true"
  tags           = ["rnic", var.vpod_name]
}
